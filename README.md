# TS-Express

Start a quick and dirty Express JS server that live-interprets Typescript, scanning your project for controllers.

**//TODO:**
-[ ] Watch and reload files
-[ ] Plugin structure (add support for things like GraphQL)
-[ ] Optionally enable Mongoose (peer dependency installation of course)
-[ ] Testing.......

## Install

```sh
npm i @rippell/ts-express
```

Then add a execution to `package.json` scripts.

```json
    ...
    "scripts": {
      "start": "ts-express",
      "debug": "ts-express-debug"
    }
    ...
```

`ts-express-debug` starts node with `--inspect` to allow debugging (go to [chrome://inspect](chrome://inspect) and click `Open dedicated DevTools for Node`).

---

Then all `.ts` files under `/src` will be found by default.
All files that export a `endpoint` object will attempt to be registered with the Express Router. 

Endpoint object should adhere to the [EndpointSet](./src/server.ts#L19) interface.
Here's an example:

```typescript
import { EndpointSet } from "@rippell/ts-express";

export const endpoints: EndpointSet = {
  '/v1/my-endpoint': {
    get:    (req, res) => { return   { cool:     'beans!'  } }, // Can Return an object
    post:   (req, res) => { res.json({ also:     'neat!'   })}, // Or send it yourself
    put:    (req, res) => { return   { too:      'many'    } },
    patch:  (req, res) => { return   { examples: 'to'      } },
    delete: (req, res) => { return   { cover:    'cleanly' } },
  },
  '/v1/another': {
    get: (req, res) => { return { moar: 'endpoints!' } }
  }
}
```

---

Optionally add a `tsexpress.ts` file at your project root, exporting a `config: TSExpressConfig` to configure TS-Express.

```typescript
import { TSExpressConfig } from '@rippell/ts-express';

export const config: TSExpressConfig = {
  port: 9999
}
``` 

See [Configuration](#configuration) below for more.

## Out of the box

### Express

Express is configured with:
* [ts-node](https://www.npmjs.com/package/ts-node) - Require, compile, and win at typescript in node
* [helmet](https://www.npmjs.com/package/helmet) - Cover the basics in security
* [body-parser](https://www.npmjs.com/package/body-parser) - Parse the body object
* [cookie-parser](https://www.npmjs.com/package/cookie-parser) - Parse and easily access cookies on the req object
* [compression](https://www.npmjs.com/package/compression) - Compress responses

### Configuration

The following configuration is used out of the box:

```typescript
{
    fileGlob:  'src/**/!(*.d).ts', // Any files under /src that end in .ts (but not .d.ts)
    host:      '0.0.0.0',          // Start and expose (convenience for containers or container-based IDEs like Eclipse-CHE)  
    port:      8080,               // Expose on 8080
    watch:     false,              // Don't set up watches by default
    verbose:   false               // Don't flood logs with TS-Express logging
}
```

See all available configuration in the [TSExpressConfig interface](./src/config.ts#L3).

As mentioned, a `tsexpress.ts` may be supplied at the project root, but all settings are overridden by environment variables in support of containerization.
See [env](./src/config.ts#L34) object for which values are pulled from the environment. 
