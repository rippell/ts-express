import * as express from 'express'
import * as helmet from 'helmet';
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as compression from 'compression';
import * as path from 'path';
import { createServer } from 'http';
import * as glob from 'glob';
import * as cors from 'cors';
import { config } from "./config";

export type Endpoint = (req: express.Request, res: express.Response) => any;
export type MethodSet = {
  get?: Endpoint,
  post?: Endpoint,
  put?: Endpoint,
  patch?: Endpoint,
  delete?: Endpoint
}
export type EndpointSet = { [key: string]: MethodSet };

let plugins = [],
    router: express.Router; // Will be replaced on live reloads

const app: express.Express = express(),
      getRouter            = (cb) => {
        router = express.Router();

        glob(config.fileGlob, { cache: false }, (er, files) => {
          files.forEach(f => {
            try {
              const exports = require(path.join(config.dir, f));

              if (exports.endpoints) {
                Object.entries(exports.endpoints).forEach(([url, methods]) => Object.entries(methods).forEach(([method, fn]) => {
                  config.log(`Registered ${ method.toUpperCase() } ${ url }`);

                  router[method.toLowerCase()](url, (req, res) => {
                    config.log('Call to ' + url);
                    const results = fn(req, res, 'test', 'stuff');
                    if (!res.headersSent) {
                      config.log('Responding with', JSON.stringify(results, null, 2));

                      res.json(results).end();
                    }
                  });
                }));
              }
            } catch (e) {
              console.error(e);
              console.error('');
            }
          });

          cb(router);
        });
      },
      constructApp         = () => {
        // Configure ExpressJS
        app.use('*', cors());
        app.use(helmet());
        app.use(compression());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(cookieParser());

        if (config.plugins && config.plugins.length) {
          plugins = config.plugins;
          cbLink();
        } else {
          applyHandlers();
        }
      },
      cbLink               = () => {
        const cb = plugins.shift();
        cb(app, config, plugins.length ? cbLink : applyHandlers);
      },
      applyHandlers = () => {
        app.get('/favicon.ico', (req, res) => {
          res.status(404).send();
        });

        if (config.staticDir) {
          app.use(express.static(path.join(config.dir, config.staticDir)));
        }

        app.use((req, res, next) => {
          router(req, res, next);
        });

        if (config.staticDir && config.isSPA) {
          app.get('*', (req, res) => {
            res.sendFile(path.join(config.dir, config.staticDir + '/index.html'));
          });
        } else if (config.staticDir && config.notFound) {
          app.get('*', (req, res) => {
            res.sendFile(path.join(config.dir, config.staticDir + '/' + config.notFound));
          });
        } else {
          app.use(
            (req: express.Request, res: express.Response): void => {
              const msg = `No controller endpoint for ${ req.url }`;
              config.log(msg);
              res.status(404).send({ error: msg });
            }
          );
        }

        start();
      },
      start                = () => {
        const server = createServer(app);
        server.listen(config.port, config.host, () => console.debug(`Listening on ${ config.host }:${ config.port }`));
      };

// Get Routes and start the server
getRouter(constructApp);

