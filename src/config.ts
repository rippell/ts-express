import * as path from 'path';
import { Express } from 'express';

export interface TSExpressConfig {
  /** Project directory - defaults to directory of terminal process */
  dir?:       string;
  /** File Glob for including files in your project - default is 'src/***\/!(*.d).ts' */
  fileGlob?:  string;
  /** Host Name/IP - default: 0.0.0.0 */
  host?:      string;
  /** Port to expose on - default: 8080 */
  port?:      number;
  /** Path to folder for static content hosting */
  staticDir?: string,
  /** Is Single Page Application - redirects all requests not fulfilled by endpoints back to {staticDir}/index.html */
  isSPA?:     boolean,
  /** File to send when a request isn't fulfilled and not an SPA */
  notFound?:  string,
  /** Turn on verbose logging for TS Express */
  verbose?:   boolean,
  log?: (...msg: any) => void, // Allows plugins to utilize logger

  plugins?: ((app: Express, config: TSExpressConfig, cb: any) => void)[]
}

const dir      = process.cwd(), // Project directory

      // Merge defaults with global.TS_EXPRESS_CONFIG -- allows for extension of TS-Express while remaining overridable at the implementation level
      defaults: TSExpressConfig = Object.assign({
        dir:       dir,
        fileGlob:  'src/**/!(*.d).ts',
        host:      '0.0.0.0',
        port:      8080,
        verbose:   false
      }, global['TS_EXPRESS_CONFIG'] || {}),

      // Gather overrides from environment variables
      env      = {
        fileGlob:  process.env.TS_EXPRESS_FILE_PATTERNS,
        host:      process.env.TS_EXPRESS_HOST,
        port:      process.env.TS_EXPRESS_PORT,
        staticDir: process.env.TS_EXPRESS_STATIC_DIR,
        isSPA:     process.env.TS_EXPRESS_STATIC_SPA,
        notFound:  process.env.TS_EXPRESS_STATIC_404
      },
      // Remove undefined's as they override in Object.assign
      envSlim  = Object.keys(env).reduce((obj, e) => {
        if (env[e]) {
          obj[e] = env[e];
        }

        return obj;
      }, {});

// Attempt to merge user override file (tsexpress.ts from project high-level)
try {
  let configFile = require(path.join(dir, 'tsexpress'));
  Object.assign(defaults, configFile.config || {}); // Just overlay into defaults
} catch (e) {}

const result = Object.assign(defaults, envSlim);
result.log = result.verbose ? console.debug : () => {};

export const config = result;
